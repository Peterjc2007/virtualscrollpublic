import { CodeDescriptionPair } from './code-description-pair';
import { FilterItem } from './filter-item';

/**
 * Container for a single item of item1 summary summary data
 */
export class SummaryItem implements FilterItem<SummaryItem> {
  
  /**
   * image to display
   */
  public image : string;

  /**
   * @constructor
   
   */
  constructor(
    public item1: CodeDescriptionPair,
    
    public item2: CodeDescriptionPair,

    public color : string
  
  ) {
    
    this.image = this.getDisplayImage();
  }

   /**
   * Test to see if the item matches a search string (we use this to filter)
   * @return {boolean} - true if it passes, false otherwise
   */
  public matchesSearchString(searchString : string) : boolean {
    return true;
   } 

  /**
   * Copies the data from another item
   * @param {item1StatusItem} other - the item to copy
   */
  public copy(other: SummaryItem): void {
    

    this.item1 = other.item1;
    this.item2 = other.item2;

    
        
    if (this.image != other.image)   {
      this.image = other.image;
    }
  }
  

  /**
   * Define how we will sort these items
   * @param {item1StatusItem} other - the other item to compare with
   * @returns 
   *    - a number < 0 if this < other
   *    - a number > 0 if this > other
   *    - 0 if this = other
   */
  public compare(other: SummaryItem): number {    
    return 1;
  }

  /**
   * Determine the display image to show for this item1
   * @param {item1SummaryItem} item - the item1 to get the image for
   */
  private getDisplayImage() : string  {    
    let fileName : string;
        
    //return `assets/img/wheel.png`;
    return 'https://avatars.io/facebook/random2' 
  }

}