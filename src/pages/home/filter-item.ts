/**
 * Interface for items we want to filter on. We will use this for items in data
 * used in the CardViewBase
 */
export interface FilterItem<T> {
  /**
   * Compare method
   * @param T - the other item to compare with
   * @returns 1 if this item is greater
   *          0 if the same
   *         -1 if the other item is greater
   */
  compare (other : T) : number;

  /**
   * Test to see if the item matches a search string (we use this to filter)
   * @return {boolean} - true if it passes, false otherwise
   */
  matchesSearchString(searchString : string) : boolean;  
}