export class CodeDescriptionPair {
  /**
   *
   */
  constructor(public code, public description) {
    
    
  }

   /**
   * Compare 2 instances for equality
   * @param {CodeDescriptionPair} cdp1 - instance to compare
   * @param {CodeDescriptionPair} cdp2 - instance to compare
   */
  public static equal(cdp1 : CodeDescriptionPair, cdp2 : CodeDescriptionPair) : boolean {
    if (cdp1 == undefined && cdp2 == undefined) {
      return true;
    }

    if (cdp1 == undefined || cdp2 == undefined) {
      return false;
    }

    return cdp1.description === cdp2.description;
  }
}
