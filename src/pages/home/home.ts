import { Component, trigger, state, style, transition, animate, NgZone, ViewChild } from '@angular/core';
import { NavController, Content } from 'ionic-angular';
import { SummaryItem } from './summary-item';
import { CodeDescriptionPair  } from './code-description-pair'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
 
})


export class HomePage {
  public filteredData : Array<SummaryItem>;
  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController) {
     this.filteredData = new Array<SummaryItem>();
     for (let i = 0; i < 100; i++) {
       let s = new SummaryItem(new CodeDescriptionPair("e" + i, 'e' + i), new CodeDescriptionPair("ef" + i, 'ef' + i),"");
       this.filteredData.push(s);
     }
  }

  public click() : void {
    this.filteredData.sort((a, b) => {
      return a.item2.description > b.item2.description ? -1 : 1
    })

  }

  public onResize() {
    try {      
      let content = this.content.getContentDimensions();
      let h = content.contentHeight; 
      let w = content.contentWidth;  

     

    } catch (error) {
      console.error(`onResize: ${error}`);
    }
  }

}
